
module.exports = {
    getMenu: function() {
        return {
            label:"Extras",
            submenu:[
                {
                    label:"Merge Groups",
                    click: (item,frontWindow) => {
                        frontWindow.webContents.send('triggerMenu', {
                            msg:'mergeGroup'
                        })
                    }
                }
            ]
        }
    }
}